岭南学院地方官员数据库
======================

数据说明
--------

-   在王贤彬博士和徐现祥博士的努力下，中山大学岭南学院产业与区域经济研究中心收集了1978-2008年间中国省区市党政正职领导人简历。其中，省区市当政正职领导人简历信息包括领导人姓名、在任省份、职位级别、开始年份、终止年份、性别、教育程度等。

-   发布时间：2013-4-12

-   引用请标注，岭南学院地方官员数据库（王贤彬、徐现祥，2013）。
