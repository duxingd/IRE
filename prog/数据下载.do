copy "https://gitee.com/arlionn/IRE/raw/master/prog/cnuse.ado" cnuse.ado
qui do "cnuse.ado"

cnuse 1986_county_dialect.dta
cnuse Administrative_Approval_Centers.dta
cnuse China_dialect_diversity_index.dta
cnuse governor_central_to_local.dta
cnuse governor_information.dta
cnuse secretary_information.dta
cnuse governor_secretary_exchange.dta
cnuse output.dta
cnuse Province_growth_target_2000-2018.dta
cnuse Province_industrial_capitalstock1978-2002.dta
