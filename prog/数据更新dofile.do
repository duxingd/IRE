
*-徐现祥 IRE 数据发布

  global p "C:\Users\Administrator\Documents\data\徐现祥公开数据\码云\data"
  
  cd $p
  
  cap mkdir data2 // 更新后的数据
  
  local f "1986_county_dialect"
  use "`f'", clear  
  label dir
  des2
  
  label data "1986年中国各县方言归属数据, https://gitee.com/arlionn/IRE"	  
  
  char _dta[A1] "Data: 1986年中国各县方言归属数据"
  char _dta[A2] "数据详情：https://gitee.com/arlionn/IRE"
  char _dta[A3] "引用：刘毓芸、徐现祥、肖泽凯，劳动力跨方言流动的倒 U 型模式，经济研究，2015，50(10): 134-146。"
   
  *-测试 
  des
  des2
  char list 
  
  save "Data2/`f'", replace  // 所有数据文件处理后，上传 Data2 文件夹中的文件
